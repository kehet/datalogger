<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{

    protected $dates = [
            'timestamp',
    ];

    protected $table = 'measurements';

    protected $fillable = [
            'timestamp',
            'reading',
    ];

}
