<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Measurement;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class MeasurementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Measurement::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'reading'   => [
                        'required',
                ],
                'timestamp' => [
                        'required',
                ],
        ]);

        if ($validator->fails()) {
            return new JsonResponse($validator->errors(), 422);
        }

        $log = [
                'request' => $request->all(),
                'client'  => [
                        'ip'        => $request->getClientIps(),
                        'useragent' => $_SERVER['HTTP_USER_AGENT'],
                ],
        ];

        Log::info("reading received " . json_encode($log));

        $measurement = new Measurement($request->all());
        $measurement->save();

        return response()->json([
            'status' => 'ok',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Measurement $measurement
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function show(Measurement $measurement)
    {
        return $measurement;
    }
}
