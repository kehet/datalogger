<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Measurement;

class PageController extends Controller
{

    public function home()
    {
        $measurements = Measurement::orderBy('timestamp', 'desc')->get();

        return view('home', compact('measurements'));
    }

}
