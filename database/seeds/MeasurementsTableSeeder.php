<?php

use App\Measurement;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MeasurementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $start = Carbon::now()->startOfMonth();
        $end   = Carbon::now()->endOfMonth();

        $day = $start->copy();

        while ($day->lte($end)) {

            factory(Measurement::class)->create([
                    'timestamp'  => $day,
                    'created_at' => $day,
                    'updated_at' => $day,
            ]);

            $day->addHour();
        }

    }
}
